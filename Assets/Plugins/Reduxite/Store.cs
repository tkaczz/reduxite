﻿using System;
#if UNITY_EDITOR || UNITY_STANDALONE
using UniRx;
#else
using System.Reactive.Subjects;
#endif

namespace Reduxite {
	public delegate TAction Dispatcher<TAction>(TAction action);

	// Based on
	// https://github.com/GuillaumeSalles/redux.NET/blob/master/src/Redux/Store.cs
	// and https://github.com/austinmao/reduxity/blob/master/Assets/Packages/Redux/Store.cs
	// Without midllewares, because they're not needed (yet...)
	/// <summary>
	/// Implementation of IStore based on BehaviorSubject <para/>
	/// If changing state is not needed from more than one source, use IObservable<T> instead
	/// </summary>
	public class Store<TState, TAction> : IStore<TState, TAction> {
		private readonly object lockObject;
		private readonly Dispatcher<TAction> dispatcher;
		private readonly IReducer<TState, TAction> reducer;
		private readonly BehaviorSubject<TState> subject;

		public IObservable<TState> StateChanges { get; }
		public TState State { get; private set; }

		public Store(IReducer<TState, TAction> reducer)
			: this(reducer, default(TState)) { }

		public Store(
			IReducer<TState, TAction> reducer,
			TState initialState
		) {
			this.lockObject = new object();
			this.dispatcher = InnerDispatch;

			this.reducer = reducer;
			this.State = initialState;
			this.subject = new BehaviorSubject<TState>(initialState);
			this.StateChanges = subject.AsObservable();
		}

		public TAction Dispatch(TAction action) {
			return dispatcher(action);
		}

		private TAction InnerDispatch(TAction action) {
			TState value;

			lock(lockObject) {
				value = reducer.Execute(State, action);
			}

			State = value;
			subject.OnNext(value);

			return action;
		}
	}
}
