﻿using NUnit.Framework;
using Reduxite;
using Reduxite.Goodies;
#if UNITY_EDITOR || UNITY_STANDALONE
using UniRx;
#else
using System.Reactive.Subjects;
#endif

namespace Tests {
	public class StoreTests {
		private CompositeDisposable disposables = new CompositeDisposable();
		private readonly FuncReducer<int, TestActions> funcReducer = new FuncReducer<int, TestActions>(
				(x, action) => {
					switch(action) {
						case TestActions.Increment:
							return x + 1;

						case TestActions.Decrement:
							return x - 1;

						default:
							return x;
					}
				}
			);

		[OneTimeTearDown]
		public void CleanUpAfterTest() {
			disposables.Clear();
		}

		[Test]
		public void CheckForGettingValue() {
			var store = new Store<int, TestActions>(funcReducer, 0);

			bool isTrue = false;

			store.StateChanges
				.Subscribe(x => {
					isTrue = true;
				})
				.AddTo(disposables);

			store.Dispatch(TestActions.Increment);

			Assert.IsTrue(isTrue);
		}

		[Test]
		public void CheckForGettingCorrectValue() {
			var store = new Store<int, TestActions>(funcReducer, 0);

			bool isTrue = false;

			store.StateChanges
				.Subscribe(x => {
					if(x == 1) {
						isTrue = true;
					}
				})
				.AddTo(disposables);

			store.Dispatch(TestActions.Increment);

			Assert.IsTrue(isTrue);
		}
	}
}
