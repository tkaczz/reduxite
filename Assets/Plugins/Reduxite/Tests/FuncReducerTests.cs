﻿using NUnit.Framework;
using Reduxite.Goodies;

namespace Tests {
	public enum TestActions {
		Increment,
		Decrement
	}

	public class FuncReducerTests {
		private FuncReducer<int, TestActions> funcReducer = new FuncReducer<int, TestActions>(
			(x, action) => {
				switch(action) {
					case TestActions.Increment:
						return x + 1;

					case TestActions.Decrement:
						return x - 1;

					default:
						return x;
				}
			}
		);

		[Test]
		public void CheckIfCallbackWorksCorrectly() {
			Assert.IsTrue(1 == funcReducer.Execute(0, TestActions.Increment));
		}
	}
}