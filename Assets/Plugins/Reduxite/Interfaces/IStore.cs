﻿using System;

namespace Reduxite {
	/// <summary>
	/// Observable with changeble state in immutable way
	/// </summary>
	public interface IStore<TState, TAction> {
		IObservable<TState> StateChanges { get; }
		TState State { get; }
		TAction Dispatch(TAction action);
	}
}
