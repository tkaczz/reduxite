﻿namespace Reduxite {
	/// <summary>
	/// Describing functionality that calculate next state based on previous
	/// <para>Args should be value types ie. structs with any needed data</para>
	/// </summary>
	/// <typeparam name="TState">Type of returned new state</typeparam>
	/// <typeparam name="TAction">Type of handled action, with possible params</typeparam>
	public interface IReducer<TState, TAction> {
		TState Execute(TState previousState, TAction action);
	}
}