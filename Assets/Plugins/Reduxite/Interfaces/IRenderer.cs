﻿namespace Reduxite {
	public interface IRenderer<TState> {
		void Render(TState state);
	}
}