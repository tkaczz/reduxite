﻿using System;

namespace Reduxite.Goodies {
	public class FuncReducer<TState, TAction> : IReducer<TState, TAction> {
		private Func<TState, TAction, TState> function;

		public FuncReducer(Func<TState, TAction, TState> func) {
			this.function = func;
		}

		public TState Execute(TState previousState, TAction action) {
			return function(previousState, action);
		}
	}
}